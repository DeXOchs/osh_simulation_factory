OSHSimulationFactory
– Automizes the execution of several simulations of the OSH.


To use the factory, simply copy the .java file into the directory where the `public static void main(String[] args)` of the targeted simulation file is. Currently the targeted file is set as `runRemsSimulationPackage.java`.

To set the simulation properties, add them to `setFactoryBase()` method.

To add simulation properties, add a new class field in the header (e.g. a List<String>) and a corrensponding entry in `getSimulationPropertiesCollection()` as described inside the method.

To avoid conflicts with versioning tools, add an entry of the OSHSimulationFactory.java to the version control ignore file (e.g. `.gitignore`).


@author Philip Ochs, 03.03.2020