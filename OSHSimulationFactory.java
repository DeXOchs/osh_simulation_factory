package osh.rems.runsimulation;

import java.io.File;
import java.time.Duration;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import osh.rems.runsimulation.runRemsSimulationPackage;

public class OSHSimulationFactory {
	// factory settings
	private static final String delimiter = ",";
	private static boolean runParallel = true;

	// simulation properties
	private static List<Calendar> simulationStartDates = new ArrayList<Calendar>();
	private static Duration simulationDuration;
	private static List<String> configurationNames = new ArrayList<String>();
	private static List<String> randomSeeds = new ArrayList<String>();
	// add more: private static List<String> anyProperty = new ArrayList<String>();

	public static void main(String[] args) {

		setFactoryBase();
		runFactory(getSimulationPropertiesCollection());
	}

	private static void setFactoryBase() {

		simulationStartDates.add(new GregorianCalendar(1970, 7, 6, 0, 0));
		// add more: simulationStartDates.add(new GregorianCalendar(1970, 7, 1, 1, 0));

		simulationDuration = Duration.ofDays(7);
		
		configurationNames.add("PU_noQU_wNONE_dABS_rMAX");
		configurationNames.add("PU_noQU_wNONE_dABS_rMEAN");
		configurationNames.add("PU_noQU_wNONE_dCUB_rMAX");
		configurationNames.add("PU_noQU_wNONE_dCUB_rMEAN");
		configurationNames.add("PU_noQU_wNONE_dSQU_rMAX");
		configurationNames.add("PU_noQU_wNONE_dSQU_rMEAN");
		configurationNames.add("PU_noQU_wLIN_dABS_rMAX");
		configurationNames.add("PU_noQU_wLIN_dABS_rMEAN");
		configurationNames.add("PU_noQU_wLIN_dCUB_rMAX");
		configurationNames.add("PU_noQU_wLIN_dCUB_rMEAN");
		configurationNames.add("PU_noQU_wLIN_dSQU_rMAX");
		configurationNames.add("PU_noQU_wLIN_dSQU_rMEAN");
		configurationNames.add("PU_noQU_wQUA_dABS_rMAX");
		configurationNames.add("PU_noQU_wQUA_dABS_rMEAN");
		configurationNames.add("PU_noQU_wQUA_dCUB_rMAX");
		configurationNames.add("PU_noQU_wQUA_dCUB_rMEAN");
		configurationNames.add("PU_noQU_wQUA_dSQU_rMAX");
		configurationNames.add("PU_noQU_wQUA_dSQU_rMEAN");
		
		configurationNames.add("PU_QU_wNONE_dABS_rMAX");
		configurationNames.add("PU_QU_wNONE_dABS_rMEAN");
		configurationNames.add("PU_QU_wNONE_dCUB_rMAX");
		configurationNames.add("PU_QU_wNONE_dCUB_rMEAN");
		configurationNames.add("PU_QU_wNONE_dSQU_rMAX");
		configurationNames.add("PU_QU_wNONE_dSQU_rMEAN");
		configurationNames.add("PU_QU_wLIN_dABS_rMAX");
		configurationNames.add("PU_QU_wLIN_dABS_rMEAN");
		configurationNames.add("PU_QU_wLIN_dCUB_rMAX");
		configurationNames.add("PU_QU_wLIN_dCUB_rMEAN");
		configurationNames.add("PU_QU_wLIN_dSQU_rMAX");
		configurationNames.add("PU_QU_wLIN_dSQU_rMEAN");
		configurationNames.add("PU_QU_wQUA_dABS_rMAX");
		configurationNames.add("PU_QU_wQUA_dABS_rMEAN");
		configurationNames.add("PU_QU_wQUA_dCUB_rMAX");
		configurationNames.add("PU_QU_wQUA_dCUB_rMEAN");
		configurationNames.add("PU_QU_wQUA_dSQU_rMAX");
		configurationNames.add("PU_QU_wQUA_dSQU_rMEAN");
		
		configurationNames.add("noPU_noQU");
		configurationNames.add("noPU_QU");
		// add more: configurationNames.add("anyConfiguration");

		//randomSeeds.add("13749851");
		//randomSeeds.add("13749852");
		randomSeeds.add("13749853");
		randomSeeds.add("13749854");
		randomSeeds.add("13749855");
		//randomSeeds.add("13749856");
		//randomSeeds.add("13749857");
		//randomSeeds.add("13749858");
		//randomSeeds.add("13749859");
		//randomSeeds.add("13749860");
		// add more: randomSeeds.add("12345678");
		
		// fill in your own properties here ...
	}

	private static List<String> getSimulationPropertiesCollection() {
		/**
		 * HOW TO ADD SINGLE PROPERTIES
		 * Use the 'mapSingleToFixed(...)' method which takes each string of a list and appends a single other string.
		 *
		 * HOW TO ADD LIST LIKE (MULTIPLE) PROPERTIES
		 * First use the 'mapListToFixed(...)' method which takes each item of a list and the list to multiply.
		 * Finally flat the stream back to a single dimension with call to 'flatMap(item -> item.stream())'.
		 */
		return simulationStartDates.stream()
			// multiply simulationStartDates (initial creation of Stream<String>)
			.map(startDate -> mapCalendarToString.apply(startDate))
			// add simulationDuration
			.map(dateRepresentation -> mapSingleToFixed(dateRepresentation, Long.toString(simulationDuration.getSeconds())))
			// multiply configurationNames
			.map(timingRepresentation -> mapListToFixed(timingRepresentation, configurationNames))
			.flatMap(item -> item.stream())
			// multiply randomSeeds
			.map(configurationRepresentation -> mapListToFixed(configurationRepresentation, randomSeeds))
			.flatMap(item -> item.stream())
			// fill in your own properties here ...
			// collect finally
			.collect(Collectors.toList());
	}

	private static void runFactory(List<String> simulationPropertiesCollection) {
		File finished = new File("finished.keep");
		if (finished.exists()) finished.delete();
		
		if (runParallel) {
			
			System.out.println("Factory starting parallel with " + simulationPropertiesCollection.size() + " configurations @ " + Runtime.getRuntime().availableProcessors() + " threads");
			ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
			
			for (int i = 0; i < simulationPropertiesCollection.size(); i++) {
				String[] configuration = simulationPropertiesCollection.get(i).split(delimiter);
				executor.schedule(
						() -> runRemsSimulationPackage.main(configuration),
						i * 30,
						TimeUnit.SECONDS);
			}
			
		    executor.shutdown();
		    try {
		    	executor.awaitTermination(31, TimeUnit.DAYS);
		    } catch (Exception e) {
				System.out.println("Factory Thread was interrupted while waiting for simulations to terminate.");
			}
		    
		} else {
			
			System.out.println("Factory starting sequentially with " + simulationPropertiesCollection.size() + " configurations @ 1 thread");
			
			simulationPropertiesCollection.forEach(simulation -> {
				Thread thread = new Thread(() -> runRemsSimulationPackage.main(simulation.split(delimiter)));
				thread.start();
				
				try {
					thread.join();
				} catch (Exception e) {
					System.out.println("Factory Thread was interrupted while waiting for a simulation to terminate.");
				}
			});
		}
		
		try {
			finished.createNewFile();
		} catch (Exception e) {
			System.out.println("Factory finished but finished.keep could not be created.");
		}
		System.out.println("All simulations have terminated. Factory closed.");
	}

	/**
	 *  Maps a date to a comma delimited string (e.g. "2020,2,2,4")
	 */
	private static Function<Calendar, String> mapCalendarToString = date -> {
		return String.join(delimiter,
				Integer.toString(date.get(Calendar.YEAR)),
				Integer.toString(date.get(Calendar.MONTH)),
				Integer.toString(date.get(Calendar.DAY_OF_MONTH)),
				Integer.toString(date.get(Calendar.HOUR_OF_DAY)));
	};

	private static String mapSingleToFixed(String fixed, String single) {
		return String.join(delimiter, fixed, single);
	}

	private static List<String> mapListToFixed(String fixed, List<String> list) {
		return list.stream().map(item -> String.join(delimiter, fixed, item)).collect(Collectors.toList());
	}
}
